using System;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat("Filemon", 50.5);
            cat.Hello();

            Dog dog = new Dog("Reksioazor", 50.5);
            dog.Fetch();

            cat.Eat();
            dog.Eat();

            Animal a = new Animal();
            Console.WriteLine(a.ToString());
        }
    }

    // Base class for AnimalsThatDrinkMilk and Dog
    public class Animal
    {
        // Properties we can use in derived classes
        public string Name { get; set; }
        public double Weight { get; set; }

        public Animal()
        {
            Console.WriteLine("Konstruktor klasy Animal");
        }

        // Construct which set Name and Weight
        public Animal(string name, double weight)
        {
            Console.WriteLine("Konstruktor klasy Animal");
            Name = name;
            Weight = weight;
        }

        // Override the ToString basic method
        public override string ToString()
        {
            return "Jestem klasą bazową, jestem wszstkim";
        }

        // Eat method execute code below with possibility of overriding in derived classes
        public virtual void Eat()
        {
            Console.WriteLine("Zwierze je kasze");
        }

        // Method we can use in derived classes
        public void Hello()
        {
            Console.WriteLine("Czesc, jestem " + Name);
        }
    }

    // Abstract class can't have own objects
    abstract class AnimalsThatDrinkMilk : Animal
    {
        // Abstract method - derived classes have to override it and create own method body
        public abstract void DrinkMilk();

        // Construct only to use in derived classes
        // Before executing AnimalsThatDrinkMilk construct call construct from Animals
        protected AnimalsThatDrinkMilk(string name, double weight) : base(name, weight)
        {

        }

    }

    // Class, that inherit from AnimalsThatDrinkMilk
    class Cat : AnimalsThatDrinkMilk
    {
        // Before executing Cat construct call construct from AnimalsThatDrinkMilk
        public Cat(string name, double weight) : base(name, weight)
        {
            Console.WriteLine("Konstruktor Cat");
        }

        // Overwritten method from AnimalsThatDrinkMilk class
        public override void DrinkMilk()
        {
            Console.WriteLine("Pij mleko będziesz wielki");
        }

        // Overwritten method from Animal class
        public override void Eat()
        {
            Console.WriteLine("Sardynki jem");
        }
    }

    // Class, that inherit from Animal
    class Dog : Animal
    {
        // Before executing Dog construct call construct from Animal
        public Dog(string name, double weight) : base(name, weight)
        {

        }

        // Overwritten method from Animal class
        public override void Eat()
        {
            // Call base Eat() method
            base.Eat();
            Console.WriteLine("Jestem psem, rodzynki jem");
        }

        public void Fetch()
        {
            Console.WriteLine("Aportuje!");
        }
    }


    // In this class we have got same things, like in Dog class and Animal class
    class ShihTzu : Dog
    {

        public ShihTzu(string name, double weight) : base(name, weight)
        {
        }
    }
}
